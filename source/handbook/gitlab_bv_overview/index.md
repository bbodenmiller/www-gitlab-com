---
layout: markdown_page
title: "GitLab B.V. Overview"
---
GitLab B.V is the company behind the [GitLab open-source project](https://gitlab.com/gitlab-org/gitlab-ce/) which is hosted on [GitLab.com](https://gitlab.com) (our free hosted service). GitLab is a Rails application providing Git repository management with fine grained access controls, code reviews, issue tracking, activity feeds, wikis and continuous integration.
GitLab B.V has 4 offerings, 3 of which are free:

1. [GitLab.com](https://about.gitlab.com/gitlab-com/) - free SaaS for public and private repositories, support can be purchased
1. [GitLab Community Edition (CE)](https://about.gitlab.com/features/#community) - free, self hosted application, support from [Community](https://about.gitlab.com/getting-help/)
1. [GitLab Enterprise Edition (EE)](https://about.gitlab.com/pricing/) - paid, self hosted application, comes with additional features and support
1. [GitLab Continuous Integration (CI)](https://about.gitlab.com/gitlab-ci/) - free, self hosted application that integrates with GitLab CE/EE. Also availble as SaaS at [ci.gitlab.com](https://ci.gitlab.com)

GitLab B.V also offers:
1. [Git and GitLab Training](https://about.gitlab.com/training/)
1. [Consulting](https://about.gitlab.com/consultancy/)
1. [Custom Development work](https://about.gitlab.com/development/)

GitLab B.V has two reseller partners in the US:
* [ReleaseTEAM](http://www.releaseteam.com)
* [WANdisco](http://www.wandisco.com/press-releases/wandisco-git-multisite-adds-gitlab-support)
